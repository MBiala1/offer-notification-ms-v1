package com.amdocs.ensemble;

import com.amdocs.ensemble.channels.ICsmServiceNotificationsInputChannel;
import com.amdocs.ensemble.tmus.starter.utils.StaticContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableBinding({
		ICsmServiceNotificationsInputChannel.class
})

@EnableDiscoveryClient
@EnableAutoConfiguration
@EnableCircuitBreaker
@RestController
@EnableFeignClients("com.amdocs")
@ComponentScan("com.amdocs")
@EnableSwagger2
public class BulkRequestApplication
{
	@Autowired
	protected StaticContextHolder staticContextHolder;

	public static void main(final String[] args)
	{
		SpringApplication.run( BulkRequestApplication.class, args);
	}
}

