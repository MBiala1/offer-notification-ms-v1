package com.amdocs.ensemble.channels;

import com.amdocs.ensemble.consts.Constants;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface ICsmServiceNotificationsInputChannel {

    @Input(Constants.CSM_SERVICE_NOTIFICATIONS)
    SubscribableChannel collectionUpdatedSuccessEvent();
}
