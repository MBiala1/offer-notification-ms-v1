package com.amdocs.ensemble.config;

import com.amdocs.ensemble.channels.ICsmServiceNotificationsInputChannel;
import org.springframework.cloud.stream.annotation.EnableBinding;

@EnableBinding({
		ICsmServiceNotificationsInputChannel.class
})
public class ChannelsConfig {

}
