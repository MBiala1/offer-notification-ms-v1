package com.amdocs.ensemble.eventlistener;

import com.amdocs.ensemble.consts.Constants;
import com.amdocs.ensemble.services.CsmServiceNotificationsConsumerService;
import com.amdocs.ensemble.tmus.starter.async.consumers.listeners.AbstractListener;
import com.amdocs.ensemble.tmus.starter.utils.CustomizedLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CsmServiceNotificationsEventListener extends AbstractListener {

    @Autowired
    protected CustomizedLogger customizedLogger;

    @StreamListener(Constants.CSM_SERVICE_NOTIFICATIONS)
    public void handle(final Message<String> message) throws Throwable {
        customizedLogger.logInfo(log,"CSM Service Notifications Event. Message Received: {}", message);
        super.handle(message, Constants.CSM_SERVICE_NOTIFICATIONS, CsmServiceNotificationsConsumerService.class);
    }

}
