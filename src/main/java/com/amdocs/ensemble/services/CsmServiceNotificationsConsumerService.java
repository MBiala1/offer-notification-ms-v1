package com.amdocs.ensemble.services;

import com.amdocs.ensemble.data.CsmServiceNotificationsMB;
import com.amdocs.ensemble.tmus.starter.async.consumers.business.AbstractGenericConsumerService;
import com.amdocs.ensemble.tmus.starter.utils.ClassInvestigator;
import com.amdocs.ensemble.tmus.starter.utils.CustomizedLogger;
import com.amdocs.ensemble.tmus.starter.utils.CustomizedObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@Scope("prototype")
public class CsmServiceNotificationsConsumerService extends AbstractGenericConsumerService<CsmServiceNotificationsMB> {

    protected CustomizedLogger customizedLogger;

    public CsmServiceNotificationsConsumerService(CustomizedObjectMapper customizedObjectMapper, ClassInvestigator classInvestigator, CustomizedLogger customizedLogger) {
        super( customizedObjectMapper, classInvestigator );
        this.customizedLogger = customizedLogger;
    }

    @Override
    protected void executeImpl() {
        customizedLogger.logDebug(log, "ServiceNotificationsConsumerService - start - message {}", CsmServiceNotificationsMB.class);
    }
}
