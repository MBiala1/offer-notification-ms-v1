package com.amdocs.ensemble.data;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CsmServiceNotificationsMB implements Serializable {
    private String ckpSeqNo;
    private String eventType;
    private String ban;
    private String subscriberNo;
    private String trxType;
    private Date activityDate;
    private String eventSts;
    private String p1;
}
